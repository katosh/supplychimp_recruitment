<?php

namespace SupplyChimp\Recruitment\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table;
use SupplyChimp\Recruitment\Enum\SupplyChimpEnum;

/**
 * @author [2019-09-05] epacha
 */
class InstallSchema implements InstallSchemaInterface
{
    /**
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     * @throws \Zend_Db_Exception
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        $this->installBlogTable($setup);
        $this->installConfigTable($setup);

        $setup->endSetup();
    }

    /**
     * @param SchemaSetupInterface $setup
     * @throws \Zend_Db_Exception
     */
    protected function installBlogTable(SchemaSetupInterface $setup)
    {
        $tableName = $setup->getTable(SupplyChimpEnum::TABLE_BLOG);

        if (true == $setup->getConnection()->isTableExists($tableName)) {
            return;
        }

        $table = $setup->getConnection()
            ->newTable($tableName)
            ->addColumn(
                'id',
                Table::TYPE_INTEGER,
                null,
                [
                    'identity' => true,
                    'unsigned' => true,
                    'nullable' => false,
                    'primary' => true,
                ],
                'ID'
            )
            ->addColumn(
                'pid',
                Table::TYPE_INTEGER,
                null,
                [
                    'nullable' => false,
                    'default' => '0',
                ],
                'Page ID'
            )
            ->addColumn(
                'date',
                Table::TYPE_INTEGER,
                null,
                [
                    'nullable' => false,
                    'default' => '0',
                ],
                'Date of event'
            )
            ->addColumn(
                'status',
                Table::TYPE_SMALLINT,
                null,
                [
                    'nullable' => false,
                    'default' => '0',
                ],
                'Status'
            )
            ->addColumn(
                'title',
                Table::TYPE_TEXT,
                null,
                [
                    'nullable' => true
                ],
                'Title'
            )
            ->addColumn(
                'content',
                Table::TYPE_TEXT,
                null,
                [
                    'nullable' => true
                ],
                'Content of post'
            )
            ->addColumn(
                'created_at',
                Table::TYPE_INTEGER,
                null,
                [
                    'nullable' => true,
                ],
                'Created At'
            )
            ->setComment('News Table')
            ->setOption('type', 'InnoDB')
            ->setOption('charset', 'utf8');

        $setup->getConnection()->createTable($table);
    }

    /**
     * @param SchemaSetupInterface $setup
     * @throws \Zend_Db_Exception
     */
    protected function installConfigTable(SchemaSetupInterface $setup)
    {
        $tableName = $setup->getTable(SupplyChimpEnum::TABLE_CONFIG);

        if (true == $setup->getConnection()->isTableExists($tableName)) {
            return;
        }

        $table = $setup->getConnection()
            ->newTable($tableName)
            ->addColumn(
                'id',
                Table::TYPE_INTEGER,
                null,
                [
                    'identity' => true,
                    'unsigned' => true,
                    'nullable' => false,
                    'primary' => true,
                ],
                'ID'
            )
            ->addColumn(
                'pid',
                Table::TYPE_INTEGER,
                null,
                [
                    'nullable' => false,
                    'default' => '0',
                ],
                'Page ID'
            )
            ->addColumn(
                'pagination',
                Table::TYPE_INTEGER,
                null,
                [
                    'nullable' => false,
                    'default' => '0',
                ],
                'Date of event'
            )
            ->addColumn(
                'status',
                Table::TYPE_SMALLINT,
                null,
                [
                    'nullable' => false,
                    'default' => '0',
                ],
                'Status'
            )
            ->addColumn(
                'title',
                Table::TYPE_TEXT,
                null,
                [
                    'nullable' => true
                ],
                'Title'
            )
            ->addColumn(
                'created_at',
                Table::TYPE_INTEGER,
                null,
                [
                    'nullable' => true,
                ],
                'Created At'
            )
            ->setComment('News Table')
            ->setOption('type', 'InnoDB')
            ->setOption('charset', 'utf8');

        $setup->getConnection()->createTable($table);
    }
}
