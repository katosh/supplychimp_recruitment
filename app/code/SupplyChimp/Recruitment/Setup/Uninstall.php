<?php

namespace SupplyChimp\Recruitment\Setup;

use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\UninstallInterface;
use SupplyChimp\Recruitment\Enum\SupplyChimpEnum;

/**
 * @author [2019-09-06] epacha
 */
class Uninstall implements UninstallInterface
{
    /**
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     */
    public function uninstall(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        $connection = $setup->getConnection();
        $connection->dropTable($connection->getTableName(SupplyChimpEnum::TABLE_BLOG));
        $connection->dropTable($connection->getTableName(SupplyChimpEnum::TABLE_CONFIG));

        $setup->endSetup();
    }
}