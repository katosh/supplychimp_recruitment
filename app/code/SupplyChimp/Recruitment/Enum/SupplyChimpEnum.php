<?php

namespace SupplyChimp\Recruitment\Enum;

/**
 * @author [2019-09-05] epacha
 */
class SupplyChimpEnum
{
    /**
     * @var string
     */
    const TABLE_BLOG = 'supplychimp_recruitment_blog';

    /**
     * @var string
     */
    const TABLE_CONFIG = 'supplychimp_recruitment_config';
}
