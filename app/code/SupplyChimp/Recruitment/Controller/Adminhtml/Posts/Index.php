<?php

namespace SupplyChimp\Recruitment\Controller\Adminhtml\Posts;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\Page;
use Magento\Framework\View\Result\PageFactory;

/**
 * @author [2019-09-06] epacha
 */
class Index extends Action
{
    /**
     * @var PageFactory
     */
    protected $resultPageFactory;

    /**
     * Constructor
     *
     * @param Context $context
     * @param PageFactory $resultPageFactory
     */
    public function __construct(Context $context, PageFactory $resultPageFactory)
    {
        $this->resultPageFactory = $resultPageFactory;

        parent::__construct($context);
    }

    /**
     * Load the page defined in view/adminhtml/layout/supplychimp_recruitment_posts_index.xml
     *
     * @return Page
     */
    public function execute(): Page
    {
        return $resultPage = $this->resultPageFactory->create();
    }
}
