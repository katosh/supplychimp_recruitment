<?php

namespace SupplyChimp\Recruitment\Controller\Adminhtml\Config;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\App\Action\HttpPostActionInterface as HttpPostActionInterface;
use Magento\Framework\Controller\Result\Redirect;
use Magento\Framework\View\Result\Page;
use Magento\Framework\View\Result\PageFactory;

/**
 * @author [2019-09-06] epacha
 */
class Post extends Action implements HttpPostActionInterface
{
    /**
     * @var PageFactory
     */
    protected $resultPageFactory;

    /**
     * Constructor
     *
     * @param Context $context
     * @param PageFactory $resultPageFactory
     */
    public function __construct(Context $context, PageFactory $resultPageFactory)
    {
        $this->resultPageFactory = $resultPageFactory;

        parent::__construct($context);
    }

    /**
     * Load the page defined in view/adminhtml/layout/supplychimp_recruitment_config_index.xml
     *
     * @return Page
     */
    public function execute()
    {
        $request = $this->getRequest();

        //if (!$request->isPost()) {
        //    return $this->resultRedirectFactory->create()->setPath('*/*/');
        //}

        $params = $request->getParams();

        echo '<pre>', print_r($request, true), '</pre>';

        //return $this->resultRedirectFactory->create()->setPath('contact/index');
        return $resultPage = $this->resultPageFactory->create();
    }
}
