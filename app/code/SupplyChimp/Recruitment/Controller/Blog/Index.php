<?php

namespace SupplyChimp\Recruitment\Controller\Blog;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\Page;
use Magento\Framework\View\Result\PageFactory;
use SupplyChimp\Recruitment\Model\PostFactory;

class Index extends Action
{
    /**
     * @var PageFactory
     */
    protected $pageFactory;

    /**
     * @var PostFactory
     */
    protected $postFactory;

    /**
     * @param Context $context
     * @param PageFactory $pageFactory
     * @param PostFactory $postFactory
     */
    public function __construct(Context $context, PageFactory $pageFactory, PostFactory $postFactory)
    {
        $this->pageFactory = $pageFactory;
        $this->postFactory = $postFactory;

        parent::__construct($context);
    }

    /**
     * Load the page defined in view/frontend/layout/supplychimp_recruitment_blog_index.xml
     *
     * @return Page
     */
    public function execute(): Page
    {
        //$post = $this->postFactory->create();
        //$collection = $post->getCollection();

        return $this->pageFactory->create();
    }
}
