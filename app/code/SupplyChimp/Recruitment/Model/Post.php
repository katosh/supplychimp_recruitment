<?php

namespace SupplyChimp\Recruitment\Model;

use Magento\Framework\DataObject\IdentityInterface;
use Magento\Framework\Model\AbstractModel;
use SupplyChimp\Recruitment\Enum\SupplyChimpEnum;

/**
 * @author [2019-09-05] epacha
 */
class Post extends AbstractModel implements IdentityInterface
{
    /**
     * @var string
     */
    const CACHE_TAG = SupplyChimpEnum::TABLE_BLOG;

    /**
     * @var string
     */
    protected $_cacheTag = SupplyChimpEnum::TABLE_BLOG;

    /**
     * @var string
     */
    protected $_eventPrefix = SupplyChimpEnum::TABLE_BLOG;

    protected function _construct()
    {
        $this->_init('SupplyChimp\Recruitment\Model\ResourceModel\Post');
    }

    /**
     * @return array|string[]
     */
    public function getIdentities()
    {
        return [ self::CACHE_TAG . '_' . $this->getId() ];
    }

    /**
     * @return array
     */
    public function getDefaultValues()
    {
        $values = [];

        return $values;
    }
}