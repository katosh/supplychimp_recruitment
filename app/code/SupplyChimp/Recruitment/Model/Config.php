<?php

namespace SupplyChimp\Recruitment\Model;

use Magento\Framework\DataObject\IdentityInterface;
use Magento\Framework\Model\AbstractModel;
use SupplyChimp\Recruitment\Enum\SupplyChimpEnum;

/**
 * @author [2019-09-05] epacha
 */
class Config extends AbstractModel implements IdentityInterface
{
    /**
     * @var string
     */
    const CACHE_TAG = SupplyChimpEnum::TABLE_CONFIG;

    /**
     * @var string
     */
    protected $_cacheTag = SupplyChimpEnum::TABLE_CONFIG;

    /**
     * @var string
     */
    protected $_eventPrefix = SupplyChimpEnum::TABLE_CONFIG;

    protected function _construct()
    {
        $this->_init('SupplyChimp\Recruitment\Model\ResourceModel\Config');
    }

    /**
     * @return array|string[]
     */
    public function getIdentities()
    {
        return [ self::CACHE_TAG . '_' . $this->getId() ];
    }

    /**
     * @return array
     */
    public function getDefaultValues()
    {
        $values = [];

        return $values;
    }
}