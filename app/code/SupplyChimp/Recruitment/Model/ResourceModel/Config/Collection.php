<?php

namespace SupplyChimp\Recruitment\Model\ResourceModel\Config;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use SupplyChimp\Recruitment\Enum\SupplyChimpEnum;

/**
 * @author [2019-09-05] epacha
 */
class Collection extends AbstractCollection
{
    protected $_idFieldName = 'id';

    protected $_eventPrefix = SupplyChimpEnum::TABLE_CONFIG;

    protected $_eventObject = 'config_collection';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('SupplyChimp\Recruitment\Model\Config', 'SupplyChimp\Recruitment\Model\ResourceModel\Config');
    }
}

