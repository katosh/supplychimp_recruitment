<?php

namespace SupplyChimp\Recruitment\Block;

use Magento\Framework\View\Element\Template;
use SupplyChimp\Recruitment\Model\PostFactory;
use SupplyChimp\Recruitment\Model\ConfigFactory;

/**
 * @author [2019-09-05] epacha
 */
class Block extends Template
{
    /**
     * @var ConfigFactory
     */
    protected $configFactory;

    /**
     * @var PostFactory
     */
    protected $postFactory;

    /**
     * @param Template\Context $context
     * @param array $data
     * @param PostFactory $postFactory
     * @param ConfigFactory $configFactory
     */
    public function __construct(
        Template\Context $context,
        PostFactory $postFactory,
        ConfigFactory $configFactory,
        array $data = []
    ) {
        $this->postFactory = $postFactory;
        $this->configFactory = $configFactory;

        parent::__construct($context, $data);

        $this->_isScopePrivate = true;
    }

    /**
     * @return array
     */
    public function getPostsAction(): array
    {
        $post = $this->postFactory->create();
        $collection = $post->getCollection();
        $collection->setOrder('date', 'DESC');

        $list = [];

        foreach ($collection as $post) {
            $list[] = (object)$post->getData();
        }

        return $list;
    }

    /**
     * @return object
     */
    public function getConfigAction()
    {
        $config = $this->configFactory->create();
        $collection = $config->getCollection();

        $item = $collection->getItemByColumnValue('pid', 1);
        $item = (object)$item->getData();

        return $item;
    }

    /**
     * @return object
     */
    public function getModuleConfigAction()
    {
        //$config = $this->configFactory->create();
        //$collection = $config->getCollection();
        //
        //$item = $collection->getItemByColumnValue('pid', 1);
        //$item = (object)$item->getData();
        //
        //return $item;

        return (object)[
            'paginationsList' => [ 5, 10, 25 ],
        ];
    }

    /**
     * Returns action url for contact form
     *
     * @return string
     */
    public function getConfigFormAction()
    {
        return $this->getUrl('supplychimp_recruitment/config/index', [ '_secure' => true ]);
    }

    /**
     * @return string
     */
    public function getFormConfigPostAction()
    {
        return $this->getUrl('supplychimp_recruitment/config/post', [ '_secure' => true ]);
    }
}
